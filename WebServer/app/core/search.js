'use strict';

const path = require('path');
const glob = require('glob');
const contentProcessors = require('../functions/contentProcessors');
const utils = require('./utils');
const pageHandler = require('./page');

let instance = null;
let stemmers = null;

function getLunr(config) {
  if (instance === null) {
    instance = require('../../libs/lunr');
    require('../../libs/lunr-languages/lunr.stemmer.support')(instance);
    require('../../libs/lunr-languages/lunr.multi')(instance);
    config.searchExtraLanguages.forEach(lang =>
      require('../../libs/lunr-languages/lunr.' + lang)(instance)
    );
  }
  return instance;
}

let idx;

function getLunrIdx(contentDir, config) {
  if (idx == null || (new Date().getTime() - idx.lastupdate.getTime() > 60 * 1000)) {

    const documents = glob
      .sync(contentDir + '**/*.md')
      .map(filePath => contentProcessors.extractDocument(
        contentDir, filePath, config.debug
      ))
      .filter(doc => doc !== null);

    idx = getLunr(config)(function () {
      this.use(getStemmers(config));
      this.field('title');
      this.field('body');
      this.ref('id');
      documents.forEach((doc) => this.add(doc), this);
    });
    console.warn("[INFO] update lunr index ");
    idx.lastupdate = new Date();
  }
  return idx;
}

function getStemmers(config) {
  if (stemmers === null) {
    const languages = ['en'].concat(config.searchExtraLanguages);
    stemmers = getLunr(config).multiLanguage.apply(null, languages);
  }
  return stemmers;
}

function handler(query, config) {
  const contentDir = utils.normalizeDir(path.normalize(config.content_dir));
  const results = getLunrIdx(contentDir, config).search(`*${query}*`);
  const searchResults = [];
  console.log("[INFO] query ", query);
  results.forEach(result => {
    const p = pageHandler(contentDir + result.ref, config);
    p.excerpt = p.excerpt.replace(new RegExp('(' + query + ')', 'gim'), '<span class="search-query">$1</span>');
    searchResults.push(p);
  });

  return searchResults;
}

exports.default = handler;
module.exports = exports.default;
