var path = require('path');
var config = require('../example/config.default.js');
function IOUtils () {
  this.getRootPath = function () {
    return path.join(__dirname, '../');
  };

  this.getMDRootPath = function () {

    return path.join(config.content_dir+'/');
  }
}


module.exports = new IOUtils();
