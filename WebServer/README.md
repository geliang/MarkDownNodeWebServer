# MarkDown Doc Service

## 环境要求：

- nodejs 版本4.0+

- git 用于更新拉取md文档内容。

- nginx(可选) Nigix用于反代80/43端口,非必选,但建议上,https+域名方便google收录,

## 服务安装和配置

#### 1、从 git 拉取源代码。

```shell
git clone https://gitee.com/geliang/MarkDownNodeWebServer.git
```

#### 2、配置端口号

默认端口为`3000`,有需要可以打开 example 目录下 multiple-instances.js 文件定位到最后，修改端口号。

```shell
vim ./example/multiple-instances.js
```

定位到如下代码，修改你想要配置的端口号。

```javascript
const server = mainApp.listen(3000, function () {
  debug('Express HTTP server listening on port ' + server.address().port);
});
```

修改 multiple-instances.js 文件为可执行权限

```shell
chmod +x multiple-instances.js
```

#### 3、配置blog内容源的自动更新

写的blog有内容上的变动,直接push到git服务器,网站立刻就更新了.原理是通过git的webhook请求去触发更新脚本.默认脚本会去pull指定的git仓库.

**安全高效** 的达到 `实时`更新Blog的目的


##### 3.1 配置更新操作的脚本

该脚本会被./webhook接口调用.
打开文件 ./example/scripts/pulldoc.sh,配置你的文档源git地址

```shell
vim ./example/scripts/pulldoc.sh

### 文件内容大致如下
#!/bin/bash
pwd
cd example
if [ ! -d "content" ];then
    git clone git:xxxxxxxxxxxxxx content
fi
cd content

git pull

```

把 pulldoc.sh 文件内容中 git 地址改为文档的 git 地址。

> **注意** 如果git仓库有密码或ssh校验,则需要配置好git的校验.

> **注意** git 地址后面一定要加 content 字段, content为文档源存放目录.

##### 3.2 配置webhook

在github/gitee的Setting中配置WebHook的地址为 yourhost.com/webhook 

下面是示例截图
![](./webhook.png)

#### 启动服务

在 源码根目录下安装项目的node依赖

```shell
npm install
```

调试开发时可以直接运行server.js启动

在root目录下使用 pm2 启动服务，服务托管于 pm2 工具，所以需要安装 pm2 工具。

```shell
## 安装 pm2
npm install pm2 -g
## 运行服务
pm2 start --name=MarkDownDocServer npm -- start
```

查看服务日志是否启动成功

```shell
pm2 logs MarkDownDocServer
# 或者使用 pm2 list 查看 列表服务。
```

打开浏览器 输入地址+端口号 看看是否开启成功。

```
http://host:port
```

#### 服务其他操作

使用 pm2 查看，删除、重启服务。

查看服务名称和ID

```
pm2 list 
```

删除服务

```
pm2 delete [id或者服务名称]
```

重启服务

```
pm2 retart [id或者服务名称]
```






