;(function ($) {

  'use strict';
  // 填充table样式
  $(document).ready(function () {

    // Add amazeui  styling to tables
    $('table').addClass('am-table am-table-bordered  am-table-hover am-table-striped  .am-table-striped am-table-compact');
  });

  // 自动展开导航栏
  $(document).ready(function (e) {
    $("li").click(function (e) {
      $(this).children("ul").toggle();
      e.stopPropagation();
    });
    $("ul li ul").toggle();
    $(".category.active").children("ul").toggle();
    $("#clicktonavi").click(function (e) {
      $("#directory_navigation_container").toggle();
      e.stopPropagation();
    });

    console.log("[INFO]  ss " + ($(".am-active").offset().top));
    // 滚动导航栏
    $('#navi_right').animate({
      scrollTop: $(".am-active").offset().top
    }, 1000);
  });

  const $backToTop = $('#back-to-top');
  if ($backToTop) {
    $(window).scroll(function () {
      if ($(window).scrollTop() > 100) {
        $backToTop.fadeIn(1000);
      } else {
        $backToTop.fadeOut(1000);
      }
    });

    $backToTop.click(function () {
      $('body,html').animate({scrollTop: 0});
    });
  }

  var initToc = function () {
    const SPACING = 20;
    const $toc = $('.post-toc');
    const $footer = $('.post-footer');

    if ($toc.length) {
      const minScrollTop = $toc.offset().top - SPACING;
      const maxScrollTop = $footer.offset().top - $toc.height() - SPACING;

      const tocState = {
        start: {
          'position': 'absolute',
          'top': minScrollTop,
        },
        process: {
          'position': 'fixed',
          'top': SPACING,
        },
        end: {
          'position': 'absolute',
          'top': maxScrollTop,
        },
      };

      $(window).scroll(function () {
        const scrollTop = $(window).scrollTop();

        if (scrollTop < minScrollTop) {
          $toc.css(tocState.start);
        } else if (scrollTop > maxScrollTop) {
          $toc.css(tocState.end);
        } else {
          $toc.css(tocState.process);
        }

        const activeTocIndex = searchActiveTocIndex(headerLinksOffsetForSearch, scrollTop);

        $($toclink).removeClass('active');
        $($tocLinkLis).removeClass('has-active');

        if (activeTocIndex !== -1) {
          $($toclink[activeTocIndex]).addClass('active');
          let ancestor = $toclink[activeTocIndex].parentNode;
          while (ancestor.tagName !== 'NAV') {
            $(ancestor).addClass('has-active');
            ancestor = ancestor.parentNode.parentNode;
          }
        }
      });
    }

    const HEADERFIX = 30;
    const $toclink = $('.toc-link');
    const $headerlink = $('.headerlink');
    const $tocLinkLis = $('.post-toc-content li');

    const headerlinkTop = $.map($headerlink, function (link) {
      return $(link).offset().top;
    });

    const headerLinksOffsetForSearch = $.map(headerlinkTop, function (offset) {
      return offset - HEADERFIX;
    });

    const searchActiveTocIndex = function (array, target) {
      for (let i = 0; i < array.length - 1; i++) {
        if (target > array[i] && target <= array[i + 1]) return i;
      }
      if (target > array[array.length - 1]) return array.length - 1;
      return -1;
    };

    $(window).scroll(function () {
      const scrollTop = $(window).scrollTop();
      const activeTocIndex = searchActiveTocIndex(headerLinksOffsetForSearch, scrollTop);

      $($toclink).removeClass('active');
      $($tocLinkLis).removeClass('has-active');

      if (activeTocIndex !== -1) {
        $($toclink[activeTocIndex]).addClass('active');
        let ancestor = $toclink[activeTocIndex].parentNode;
        while (ancestor.tagName !== 'NAV') {
          $(ancestor).addClass('has-active');
          ancestor = ancestor.parentNode.parentNode;
        }
      }
    });
  };
  initToc();
})(jQuery);
