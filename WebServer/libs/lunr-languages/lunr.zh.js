/**
 * export the module via AMD, CommonJS or as a browser global
 * Export code from https://github.com/umdjs/umd/blob/master/returnExports.js
 */
;(function (root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define(factory)
    } else if (typeof exports === 'object') {
        /**
         * Node. Does not work with strict CommonJS, but
         * only CommonJS-like environments that support module.exports,
         * like Node.
         */
        module.exports = factory()
    } else {
        // Browser globals (root is window)
        factory()(root.lunr);
    }
}(this, function () {
    /**
     * Just return a value to define the module export.
     * This example returns an object, but the module
     * can return a function as the exported value.
     */
    return function(lunr) {
        /* throw error if lunr is not yet included */
        if ('undefined' === typeof lunr) {
            throw new Error('Lunr is not present. Please include / require Lunr before this script.');
        }

        /* throw error if lunr stemmer support is not yet included */
        if ('undefined' === typeof lunr.stemmerSupport) {
            throw new Error('Lunr stemmer support is not present. Please include / require Lunr stemmer support before this script.');
        }

        /* register specific locale function */
        lunr.zh = function () {
            this.pipeline.reset();
            this.pipeline.add(
                lunr.zh.trimmer,
                lunr.zh.stopWordFilter,
                lunr.zh.stemmer
            );

            // for lunr version 2
            // this is necessary so that every searched word is also stemmed before
            // in lunr <= 1 this is not needed, as it is done using the normal pipeline
            if (this.searchPipeline) {
                this.searchPipeline.reset();
                this.searchPipeline.add(lunr.zh.stemmer)
            }
        };

        /* lunr trimmer function */
        lunr.zh.wordCharacters = "\u4E00-\u9FA5\uF900-\uFA2D";
        lunr.zh.trimmer = lunr.trimmerSupport.generateTrimmer(lunr.zh.wordCharacters);

        lunr.Pipeline.registerFunction(lunr.zh.trimmer, 'trimmer-zh');

        /* lunr stemmer function */
        lunr.zh.stemmer = (function() {
            /* create the wrapped stemmer object */
            var Among = lunr.stemmerSupport.Among,
                SnowballProgram = lunr.stemmerSupport.SnowballProgram,
                st = new function ChineseStemmer(){
                  var sbp = new SnowballProgram();
                  this.setCurrent = function(word) {
                    sbp.setCurrent(word);
                  };
                  this.getCurrent = function() {
                    return sbp.getCurrent();
                  };
                  this.stem = function() {
                    var v_1 = sbp.cursor;
                    // r_prelude();
                    sbp.cursor = v_1;
                    // r_mark_regions();
                    sbp.limit_backward = v_1;
                    sbp.cursor = sbp.limit;
                    // r_standard_suffix();
                    sbp.cursor = sbp.limit_backward;
                    // r_postlude();
                    return true;
                  }
                };
/*// 载入模块
          var Segment = require('node-segment').Segment;
// 创建实例
          var segment = new Segment();
// 使用默认的识别模块及字典，载入字典文件需要1秒，仅初始化时执行一次即可
          segment.useDefault();*/
            /* and return a function that stems a word for the current locale */
            return function(token) {
                // for lunr version 2
                if (typeof token.update === "function") {
                    return token.update(function (word) {
                        st.setCurrent(word);
                        st.stem();
                        return st.getCurrent();
                      // var wordList = segment.doSegment(word);
                      // return wordList.map(function (token) {
                      //   return token.w.toLowerCase()
                      // })
                    })
                } else { // for lunr version <= 1
                    st.setCurrent(token);
                    st.stem();
                    return st.getCurrent();
                  // var wordList = segment.doSegment(token);
                  // return wordList.map(function (token) {
                  //   return token.w.toLowerCase()
                  // })
                }
            }
        })();

        lunr.Pipeline.registerFunction(lunr.zh.stemmer, 'stemmer-zh');

        lunr.zh.stopWordFilter = lunr.generateStopWordFilter(''.split(' '));

        lunr.Pipeline.registerFunction(lunr.zh.stopWordFilter, 'stopWordFilter-zh');
    };
}))
