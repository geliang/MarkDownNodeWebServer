# 支持MarkDown(MD)的Blog博客Web系统 

此博客基于Raneto二次开发,支持手机和桌面浏览器

> Nodejs一键部署,文章格式MarkDown,以git(github/gitee)仓库为内容源,WebHook自动更新,无数据库依赖

示例网站(`白色主题`):[http://fluttergo.com](http://fluttergo.com/) 

示例网站(`灰色主题`) :[http://erdange.com](http://erdange.com/) 

##### PC版本截图

![PC版本](screenshot.png)

##### 手机截图

![手机版](Snipaste_2019-05-09_22-51-56.gif)

---

## 特性

相比Raneto,改动如下

特性:

- 支持中英文关键字搜索 (update 2019-05-07)

- 可自定义主题(自带2套主题,随意切换)

- 支持git仓库作为bolg的内容源,并提供git webhook做到推送即发布

- 支持目录导航,排序,中英文路径,路径别名

- 支持相对地址的图文链接(无需额外图床)

- 支持文章排序,文章内段落跳转,标题别名

- 响应式支持手机和桌面浏览器

## RoadMap

- 留言评论 (候选方案 https://github.com/imsun/gitment)


> 如有一些新的想法,欢迎PR或者留言

## 安装说明
熟悉node那套的,几分钟就可以跑起来,详细见 [./WebServer/README.md](./WebServer/README.md)

主题版权等配置项的修改在 (./WebServer/example/config.default.js)

源代码地址:https://gitee.com/geliang/MarkDownNodeWebServer

> Ranto官网: [http://docs.raneto.com](http://docs.raneto.com) 
>
> to see a demo and more...

